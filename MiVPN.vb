Imports System.IO

Public Class MiVPN
    Dim first As Boolean = True
    Dim intRandomNumber As Integer = 250
    Dim posX As Integer
    Dim posY As Integer
    Dim drag As Boolean
    Dim CustomToolTip As System.Windows.Forms.ToolTip = New System.Windows.Forms.ToolTip()

    Private Sub Enable_Click(sender As System.Object, e As System.EventArgs) Handles Enable.Click
        newIP()
        Enable.Visible = False
        Fresh.Visible = True
        AnimateWin()
    End Sub


    Private Sub Btn_Exit_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Exit.Click
        StopNexit()
    End Sub

    Private Sub Animation_DoubleClick(sender As Object, e As System.EventArgs) Handles Animation.DoubleClick
        If Animation.Dock = DockStyle.Fill Then
            Animation.Dock = DockStyle.None
        Else
            Animation.Dock = DockStyle.Fill
        End If
        Me.TopMost = True
    End Sub

    Private Sub Animation_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Animation.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
        If e.Button = MouseButtons.Right Then
            'MsgBox("Right Button Clicked")
            AnimateWin()
        End If
    End Sub

    Private Sub Animation_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Animation.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
    End Sub

    Private Sub Animation_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Animation.MouseUp
        drag = False
    End Sub

    Private Sub MiVPN_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        StopNexit()
    End Sub

    Private Sub MiVPN_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.TopMost = True
        Dim x As Integer
        Dim y As Integer
        x = Screen.PrimaryScreen.WorkingArea.Width
        y = Screen.PrimaryScreen.WorkingArea.Height - Me.Height
        Do Until x = Screen.PrimaryScreen.WorkingArea.Width - Me.Width
            x = x - 1
            Me.Location = New Point(x, y)
        Loop
    End Sub

    Public Sub newIP()
        intRandomNumber = GetRandom(1, 255)
        Try
            Label1.Text = "Connected to server: " & intRandomNumber
            Process.Start("CMD", "/C netsh interface ipv4 set address name=2 source=static address=192.168.1." & intRandomNumber & " mask=255.255.255.0 gateway=192.168.1.1")
            'Process.Start("CMD", "/C netsh interface ipv4 set address name=2 source=dhcp")
        Catch ex As Exception
            MsgBox("Error!")
        End Try
        Animation.Enabled = True
    End Sub

    Public Sub StopNexit()
        Try
            Process.Start("CMD", "/C netsh interface ipv4 set address name=2 source=static address=192.168.1.0 mask=255.0.0.0")
        Catch ex As Exception
            MsgBox("Error!")
        End Try
        Environment.Exit(1)
    End Sub

    Public Function GetRandom(ByVal min As Integer, ByVal max As Integer) As Integer
        Static staticRandomGenerator As New System.Random
        max += 1
        Return staticRandomGenerator.Next(If(min > max, max, min), If(min > max, min, max))
    End Function

    Private Sub Fresh_Click(sender As System.Object, e As System.EventArgs) Handles Fresh.Click
        newIP()
    End Sub

    Private Sub Btn_Minimize_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Minimize.Click
        Me.ShowInTaskbar = False
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Fresh_MouseHover(sender As Object, e As System.EventArgs) Handles Fresh.MouseHover
        CustomToolTip.SetToolTip(Fresh, "Refresh")
    End Sub
    Public Sub AnimateWin()
        Dim x As Integer
        Dim y As Integer
        x = Screen.PrimaryScreen.WorkingArea.Width
        y = Screen.PrimaryScreen.WorkingArea.Height - Me.Height
        Do Until x = Screen.PrimaryScreen.WorkingArea.Width - Me.Width + 40
            x = x - 1
            Me.Location = New Point(x, y)
        Loop
    End Sub
    
End Class
